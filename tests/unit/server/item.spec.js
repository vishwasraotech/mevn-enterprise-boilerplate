const request = require("supertest");
const path = require("path");
const app = require(path.join(__dirname, "../../../server"));
const { seedItems, populateItems } = require("./seed");
const mongoose = require("mongoose");

beforeEach(populateItems);

afterAll(async () => {
  await mongoose.connection.close();
});

describe("GET /items", () => {
  it("should get all items", async () => {
    const res = await request(app)
      .get("/items")
      .expect(200);
    expect(res.body.length).toBe(seedItems.length);
  });
});

describe("GET /itemByAuthor", () => {
  it("should get 1 item", async () => {
    const res = await request(app)
      .get("/itemByAuthor?author=Author 4")
      .expect(200);
    expect(res.body.title).toBe(seedItems[3].title);
  });
});

describe("GET /itemByAuthor", () => {
  it("Invalid author", async () => {
    const res = await request(app)
      .get("/itemByAuthor?author=someAuthor")
      .expect(200);
    expect(res.body).toBe(null);
  });
});