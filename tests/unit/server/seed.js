const path = require("path");
const Item = require(path.join(__dirname, "../../../server/models/item"));

const seedItems = [
  {
    title: "Test item 1",
    author: "Author 1"
  },
  {
    title: "Test item 2",
    author: "Author 2"
  },
  {
    title: "Test item 3",
    author: "Author 3"
  },
  {
    title: "Test item 4",
    author: "Author 4"
  }
];

const populateItems = async () => {
  await Item.deleteMany();
  await Item.insertMany(seedItems);
};

module.exports = { seedItems, populateItems };
