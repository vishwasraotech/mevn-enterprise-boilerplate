const express = require("express")
const router = express.Router()

const items = require("./items")

items(router)

module.exports = router