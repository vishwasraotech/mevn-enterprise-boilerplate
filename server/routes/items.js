
const ItemController = require("../controllers/ItemController");
const itemController = new ItemController()

function register(router){
    router.get("/items", getItems)
    router.get("/itemByAuthor", getItemByAuthor)
}

async function getItems(req, res){
const items = await itemController.getAllItems()
res.setHeader('Content-Type', 'application/json');
res.status(200).end(JSON.stringify(items))
}

async function getItemByAuthor(req, res){
    let author = req.query.author;
    const item = await itemController.getItemByAuthor(author)
    res.setHeader('Content-Type', 'application/json');
    res.status(200).end(JSON.stringify(item))
    }

module.exports = register;