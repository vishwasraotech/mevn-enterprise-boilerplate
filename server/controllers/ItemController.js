const Item = require("../models/item");

class ItemController {

    async getAllItems() {
        try {
            const items = await Item.find();
            return items;
        } catch (err) {
            console.log("Error while getting items, error is" + err)
        }
    }

    async getItemByAuthor(author) {
        try {
            const item = await Item.findOne({ "author": { "$eq": author }});
            return item;
        } catch (err) {
            console.log("Error while getting item, error is" + err)
        }
    }
}

module.exports = ItemController